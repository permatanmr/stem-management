import os
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

def validate_file_extension(value):
    ext = os.path.splitext(value.name)[1]  # Get file extension
    valid_extensions = ['.jpg', '.jpeg', '.png', '.pdf', '.docx', '.ppt', '.pptx']  # Define allowed extensions
    if not ext.lower() in valid_extensions:
        raise ValidationError(_('Unsupported file extension. Allowed extensions are: %(valid)s'),
                              params={'valid': ', '.join(valid_extensions)})
